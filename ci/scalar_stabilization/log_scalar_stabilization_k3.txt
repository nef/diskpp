Tested degree: 3
Test HHO-stabilization operator
Face order: k and Cell order: k
Mesh under test: triangles on generic mesh
  Testing degree 3 (expected rate is 4)
    2.27560e-01    4.55845e-05     -- 
    1.13780e-01    2.86945e-06    3.99    [ OK ]
    5.68900e-02    1.85158e-07    3.95    [ OK ]
    2.84450e-02    5.08815e-08    1.86    [FAIL]
    1.42225e-02    -nan    -nan    [FAIL]
Mesh under test: polygons on generic mesh
  Testing degree 3 (expected rate is 4)
    3.34911e-01    2.17600e-04     -- 
    1.75895e-01    1.45023e-05    4.21    [ OK ]
    9.06001e-02    9.28408e-07    4.14    [ OK ]
    4.60523e-02    -nan    -nan    [FAIL]
    2.32276e-02    3.14977e-07    -nan    [FAIL]
Mesh under test: quads on generic mesh
  Testing degree 3 (expected rate is 4)
    3.53553e-01    5.17368e-04     -- 
    1.76777e-01    3.28874e-05    3.98    [ OK ]
    8.83883e-02    2.06475e-06    3.99    [ OK ]
    4.41942e-02    1.66651e-07    3.63    [ OK ]
    2.20971e-02    1.87005e-07    -0.166    [FAIL]
Mesh under test: 2D cartesian mesh (DiSk++)
  Testing degree 3 (expected rate is 4)
    3.53553e-01    5.17368e-04     -- 
    1.76777e-01    3.28874e-05    3.98    [ OK ]
    8.83883e-02    2.06461e-06    3.99    [ OK ]
    4.41942e-02    1.43675e-07    3.84    [ OK ]
Face order: k and Cell order: k+1
Mesh under test: triangles on generic mesh
  Testing degree 0 (expected rate is 1)
    2.27560e-01    1.35006e-01     -- 
    1.13780e-01    6.81566e-02    0.986    [ OK ]
    5.68900e-02    3.41605e-02    0.997    [ OK ]
    2.84450e-02    1.70905e-02    0.999    [ OK ]
    1.42225e-02    8.54655e-03    1    [ OK ]
  Testing degree 1 (expected rate is 2)
    2.27560e-01    1.27507e-02     -- 
    1.13780e-01    3.21405e-03    1.99    [ OK ]
    5.68900e-02    8.05169e-04    2    [ OK ]
    2.84450e-02    2.01396e-04    2    [ OK ]
    1.42225e-02    5.03563e-05    2    [ OK ]
Mesh under test: polygons on generic mesh
  Testing degree 0 (expected rate is 1)
    3.34911e-01    2.98128e-01     -- 
    1.75895e-01    1.65259e-01    0.916    [ OK ]
    9.06001e-02    8.61899e-02    0.981    [ OK ]
    4.60523e-02    4.39169e-02    0.996    [ OK ]
    2.32276e-02    2.21549e-02    1    [ OK ]
  Testing degree 1 (expected rate is 2)
    3.34911e-01    3.89470e-02     -- 
    1.75895e-01    1.04399e-02    2.04    [ OK ]
    9.06001e-02    2.68643e-03    2.05    [ OK ]
    4.60523e-02    6.80402e-04    2.03    [ OK ]
    2.32276e-02    1.71149e-04    2.02    [ OK ]
Mesh under test: quads on generic mesh
  Testing degree 0 (expected rate is 1)
    3.53553e-01    1.97912e-01     -- 
    1.76777e-01    1.01834e-01    0.959    [ OK ]
    8.83883e-02    5.12821e-02    0.99    [ OK ]
    4.41942e-02    2.56868e-02    0.997    [ OK ]
    2.20971e-02    1.28491e-02    0.999    [ OK ]
  Testing degree 1 (expected rate is 2)
    3.53553e-01    4.79451e-02     -- 
    1.76777e-01    1.22463e-02    1.97    [ OK ]
    8.83883e-02    3.07802e-03    1.99    [ OK ]
    4.41942e-02    7.70536e-04    2    [ OK ]
    2.20971e-02    1.92699e-04    2    [ OK ]
Mesh under test: 2D cartesian mesh (DiSk++)
  Testing degree 0 (expected rate is 1)
    3.53553e-01    1.97912e-01     -- 
    1.76777e-01    1.01834e-01    0.959    [ OK ]
    8.83883e-02    5.12821e-02    0.99    [ OK ]
    4.41942e-02    2.56868e-02    0.997    [ OK ]
  Testing degree 1 (expected rate is 2)
    3.53553e-01    4.79451e-02     -- 
    1.76777e-01    1.22463e-02    1.97    [ OK ]
    8.83883e-02    3.07802e-03    1.99    [ OK ]
    4.41942e-02    7.70536e-04    2    [ OK ]
Face order: k and Cell order: k-1
Mesh under test: triangles on generic mesh
  Testing degree 3 (expected rate is 4)
    2.27560e-01    1.97151e-05     -- 
    1.13780e-01    1.24152e-06    3.99    [ OK ]
    5.68900e-02    7.77562e-08    4    [ OK ]
    2.84450e-02    4.74183e-09    4.04    [ OK ]
    1.42225e-02    -nan    -nan    [FAIL]
Mesh under test: polygons on generic mesh
  Testing degree 3 (expected rate is 4)
    3.34911e-01    1.06871e-04     -- 
    1.75895e-01    7.23841e-06    4.18    [ OK ]
    9.06001e-02    4.67746e-07    4.13    [ OK ]
    4.60523e-02    -nan    -nan    [FAIL]
    2.32276e-02    -nan    -nan    [FAIL]
Mesh under test: quads on generic mesh
  Testing degree 3 (expected rate is 4)
    3.53553e-01    3.42728e-04     -- 
    1.76777e-01    2.17692e-05    3.98    [ OK ]
    8.83883e-02    1.36613e-06    3.99    [ OK ]
    4.41942e-02    9.50085e-08    3.85    [ OK ]
    2.20971e-02    -nan    -nan    [FAIL]
Mesh under test: 2D cartesian mesh (DiSk++)
  Testing degree 3 (expected rate is 4)
    3.53553e-01    3.42728e-04     -- 
    1.76777e-01    2.17692e-05    3.98    [ OK ]
    8.83883e-02    1.36592e-06    3.99    [ OK ]
    4.41942e-02    8.39746e-08    4.02    [ OK ]
Test HDG-stabilization operator
Face order: k and Cell order: k+1
Mesh under test: triangles on generic mesh
  Testing degree 3 (expected rate is 4)
    2.27560e-01    4.42259e-05     -- 
    1.13780e-01    2.78236e-06    3.99    [ OK ]
    5.68900e-02    9.16520e-08    4.92    [FAIL]
    2.84450e-02    -nan    -nan    [FAIL]
    1.42225e-02    4.83586e-07    -nan    [FAIL]
Mesh under test: polygons on generic mesh
  Testing degree 3 (expected rate is 4)
    3.34911e-01    2.16908e-04     -- 
    1.75895e-01    1.44799e-05    4.2    [ OK ]
    9.06001e-02    9.34768e-07    4.13    [ OK ]
    4.60523e-02    1.70002e-07    2.52    [FAIL]
    2.32276e-02    -nan    -nan    [FAIL]
Mesh under test: quads on generic mesh
  Testing degree 3 (expected rate is 4)
    3.53553e-01    5.18127e-04     -- 
    1.76777e-01    3.28995e-05    3.98    [ OK ]
    8.83883e-02    2.06745e-06    3.99    [ OK ]
    4.41942e-02    2.88946e-07    2.84    [FAIL]
    2.20971e-02    5.32171e-07    -0.881    [FAIL]
Mesh under test: 2D cartesian mesh (DiSk++)
  Testing degree 3 (expected rate is 4)
    3.53553e-01    5.18127e-04     -- 
    1.76777e-01    3.28995e-05    3.98    [ OK ]
    8.83883e-02    2.06800e-06    3.99    [ OK ]
    4.41942e-02    2.94312e-07    2.81    [FAIL]
Test dG-stabilization operator
Face order: k and Cell order: k
Mesh under test: triangles on generic mesh
  Testing degree 3 (expected rate is 3)
    2.27560e-01    9.19128e-04     -- 
    1.13780e-01    1.15694e-04    2.99    [ OK ]
    5.68900e-02    1.44866e-05    3    [ OK ]
    2.84450e-02    1.83221e-06    2.98    [ OK ]
    1.42225e-02    -nan    -nan    [FAIL]
Mesh under test: polygons on generic mesh
  Testing degree 3 (expected rate is 3)
    3.34911e-01    3.49458e-03     -- 
    1.75895e-01    4.60089e-04    3.15    [ OK ]
    9.06001e-02    5.87779e-05    3.1    [ OK ]
    4.60523e-02    7.42106e-06    3.06    [ OK ]
    2.32276e-02    9.95789e-07    2.93    [ OK ]
Mesh under test: quads on generic mesh
  Testing degree 3 (expected rate is 3)
    3.53553e-01    6.34835e-03     -- 
    1.76777e-01    8.08191e-04    2.97    [ OK ]
    8.83883e-02    1.01486e-04    2.99    [ OK ]
    4.41942e-02    1.27028e-05    3    [ OK ]
    2.20971e-02    1.65895e-06    2.94    [ OK ]
Mesh under test: 2D cartesian mesh (DiSk++)
  Testing degree 3 (expected rate is 3)
    3.53553e-01    6.34835e-03     -- 
    1.76777e-01    8.08191e-04    2.97    [ OK ]
    8.83883e-02    1.01486e-04    2.99    [ OK ]
    4.41942e-02    1.27030e-05    3    [ OK ]
